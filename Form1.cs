﻿using com.eninetworks.enitv.horarios;
using com.eninetworks.utilerias.excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LeerHorario
{
    public partial class Form1 : Form
    {
        string urlPath = "";
        public Form1()
        {
            InitializeComponent();
            selectAutomatic();
            
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {

                TypeGuide typeGuideSelected=(TypeGuide)typeGuide.SelectedItem;
                if (urlPath!="" && txtCANAL.Text!="" && typeGuideSelected.nameGuide!= "Seleccione")
                {
                    new Manager(urlPath, txtCANAL.Text, typeGuideSelected).SaveXMLTV();
                    MessageBox.Show("Transformado correctamente");
                }
                else
                {
                    MessageBox.Show("Falta información");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
            }
        }
        private void Button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                txtFILENAME.Text = openFileDialog1.SafeFileName;
                urlPath = openFileDialog1.FileName;
                typeGuide.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void bunifuGradientPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtFILENAME_TextChanged(object sender, EventArgs e)
        {

        }

        private void selectAutomatic()
        {
            List<TypeGuide> typeGuideList = new List<TypeGuide>();
            typeGuideList.Add(new TypeGuide("Seleccione", "row"));
            typeGuideList.Add(new TypeGuide("SPI", "row"));
            typeGuideList.Add(new TypeGuide("DePelicula", "row"));
            typeGuideList.Add(new TypeGuide("AztecaClic", "compuesto"));
            typeGuideList.Add(new TypeGuide("EDGE", "compuesto"));
            typeGuideList.Add(new TypeGuide("Bandamax", "compuesto"));
            typeGuideList.Add(new TypeGuide("BitMe", "compuesto"));
            var bindingSource1 = new BindingSource();
            bindingSource1.DataSource = typeGuideList;
            typeGuide.DataSource = bindingSource1.DataSource;
            typeGuide.DisplayMember = "nameGuide";
            typeGuide.SelectedIndex = 0;
        }

        private void bunifuImageButton1_Click(object sender, EventArgs e)
        {

        }

        private void bunifuImageButton1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
